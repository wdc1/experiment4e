import pandas as pd
# 读取心脏病数据集
data = pd.read_csv("/processed cleveland3.csv")

# 使用map()函数对target列中的值进行转换
data['target'] = data['target'].map(lambda x: 1 if x in [2, 3, 4] else x)

# 打印数据转换后的结果
print("转换后的数据集：")
print(data)
data.to_csv('processed cleveland4.csv',index=False)