import pandas as pd
import matplotlib.pyplot as plt

# 读取心脏病数据集
data = pd.read_csv("/processed cleveland2.csv")

# 绘制箱线图
plt.figure(figsize=(10, 6))
data.boxplot()
plt.title('Boxplot1 of Heart Disease Dataset')
plt.xticks(rotation=90)
plt.show()

# 检测处理差异较大的异常值
data1 = data[['chol','thalach']]
Q1 = data1.quantile(0.25)
Q3 = data1.quantile(0.75)
IQR = Q3 - Q1
lower_bound = Q1 - 1.5 * IQR
upper_bound = Q3 + 1.5 * IQR
outliers = ((data1 < lower_bound) | (data1 > upper_bound)).any(axis=1)

# 删除异常值
data_no_outliers = data[~outliers]

# 绘制箱线图（处理完异常值后）
plt.figure(figsize=(10, 6))
data_no_outliers.boxplot()
plt.title('Boxplot2 of Heart Disease Dataset')
plt.xticks(rotation=90)
plt.show()

# 打印处理后的数据集
print("处理后的数据集：")
print(data_no_outliers)
data_no_outliers.to_csv('processed cleveland3.csv',index=False)