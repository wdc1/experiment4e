import pandas as pd
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
from sklearn.base import clone
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# 加载数据集
df = pd.read_csv("D:\importance\py\pythonProject5\processed cleveland4.csv")

# 划分特征和标签
X = df.drop('target', axis=1)
y = df['target']

# 划分训练集和测试集
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# 特征缩放
scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

# 定义基分类器：KNN
knn = KNeighborsClassifier(n_neighbors=27)

# 定义基分类器：朴素贝叶斯
gnb = GaussianNB()

# 训练基分类器
knn.fit(X_train_scaled, y_train)
gnb.fit(X_train_scaled, y_train)

# 获取基分类器在训练集上的预测（作为第二层分类器的特征）
train_predictions_knn = knn.predict_proba(X_train_scaled)[:, 1]  # 取正类的概率
train_predictions_gnb = gnb.predict_proba(X_train_scaled)[:, 1]  # 取正类的概率

# 堆叠特征
stacked_train_features = np.column_stack((train_predictions_knn, train_predictions_gnb))

# 定义第二层分类器
stacking_clf = LogisticRegression()

# 使用堆叠特征训练第二层分类器
stacking_clf.fit(stacked_train_features, y_train)

# 预测测试集
test_predictions_knn = knn.predict_proba(X_test_scaled)[:, 1]
test_predictions_gnb = gnb.predict_proba(X_test_scaled)[:, 1]
stacked_test_features = np.column_stack((test_predictions_knn, test_predictions_gnb))
y_pred = stacking_clf.predict(stacked_test_features)

# 评估模型
accuracy = accuracy_score(y_test, y_pred)
print(f"Stacking Accuracy: {accuracy}")
print(classification_report(y_test, y_pred))

# 绘制混淆矩阵
cm = confusion_matrix(y_test, y_pred)
plt.figure(figsize=(10, 7))
sns.heatmap(cm, annot=True, fmt='d', cmap='Blues', xticklabels=['Negative', 'Positive'],
            yticklabels=['Negative', 'Positive'])
plt.xlabel('Predicted')
plt.ylabel('Truth')
plt.title('Confusion Matrix')
plt.show()

# 定义基准模型：单独的高斯朴素贝叶斯
gnb_base = GaussianNB()
gnb_base_score = cross_val_score(gnb_base, X_train_scaled, y_train, cv=5, scoring='accuracy').mean()
print(f"Baseline Accuracy (GaussianNB): {gnb_base_score}")


from sklearn.metrics import roc_curve, auc, precision_recall_curve, average_precision_score
from matplotlib import pyplot as plt
from scipy import interp

# 计算堆叠模型的ROC曲线
fpr_stacking, tpr_stacking, thresholds_stacking = roc_curve(y_test, y_pred)
roc_auc_stacking = auc(fpr_stacking, tpr_stacking)

# 计算基准模型的ROC曲线
y_pred_gnb_base = gnb_base.fit(X_train_scaled, y_train).predict_proba(X_test_scaled)[:, 1]
fpr_gnb_base, tpr_gnb_base, thresholds_gnb_base = roc_curve(y_test, y_pred_gnb_base)
roc_auc_gnb_base = auc(fpr_gnb_base, tpr_gnb_base)

# 绘制ROC曲线
plt.figure(figsize=(10, 7))
lw = 2
plt.plot(fpr_stacking, tpr_stacking, color='darkorange', lw=lw, label='Stacking (area = %0.2f)' % roc_auc_stacking)
plt.plot(fpr_gnb_base, tpr_gnb_base, color='navy', lw=lw, label='GaussianNB (area = %0.2f)' % roc_auc_gnb_base)
plt.plot([0, 1], [0, 1], color='gray', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic')
plt.legend(loc="lower right")
plt.show()

# 计算堆叠模型的PR曲线
precision_stacking, recall_stacking, _ = precision_recall_curve(y_test, y_pred)
average_precision_stacking = average_precision_score(y_test, y_pred)

# 计算基准模型的PR曲线
precision_gnb_base, recall_gnb_base, _ = precision_recall_curve(y_test, y_pred_gnb_base)
average_precision_gnb_base = average_precision_score(y_test, y_pred_gnb_base)

# 绘制PR曲线
plt.figure(figsize=(10, 7))
plt.plot(recall_stacking, precision_stacking, color='darkorange', lw=lw,
         label='Stacking (AP = %0.2f)' % average_precision_stacking)
plt.plot(recall_gnb_base, precision_gnb_base, color='navy', lw=lw,
         label='GaussianNB (AP = %0.2f)' % average_precision_gnb_base)
plt.xlabel('Recall')
plt.ylabel('Precision')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.title('Precision-Recall Curve')
plt.legend(loc="lower left")
plt.show()


# 特征列名
example_feature_names = ['age', 'sex', 'cp', 'trestbps', 'chol', 'fbs', 'restecg', 'thalach', 'exang', 'oldpeak',
                         'slope', 'ca', 'thal']

# 创建一个模拟案例的字典
example_case = {
    'age': 65,
    'sex': 1,  # 假设1为男性
    'cp': 3,  # 胸痛类型
    'trestbps': 130,  # 静息血压
    'chol': 245,  # 胆固醇
    'fbs': 0,  # 空腹血糖是否大于120mg/dl
    'restecg': 2,  # 静息心电图结果
    'thalach': 150,  # 最大心率
    'exang': 0,  # 运动诱发心绞痛
    'oldpeak': 2.3,  # 运动ST段压低
    'slope': 2,  # ST段下降的斜率
    'ca': 0,  # 主要的血管数量（0-3）
    'thal': 3  # 缺陷类型
}

# 将模拟案例转换为DataFrame
example_df = pd.DataFrame([example_case], columns=example_feature_names)

# 对模拟案例进行特征缩放（使用相同的scaler）
example_scaled = scaler.transform(example_df)

# 使用KNN和朴素贝叶斯进行预测
example_predictions_knn = knn.predict_proba(example_scaled)[:, 1]
example_predictions_gnb = gnb.predict_proba(example_scaled)[:, 1]

# 堆叠特征
stacked_example_features = np.column_stack((example_predictions_knn, example_predictions_gnb))

# 使用堆叠分类器进行预测
example_prediction = stacking_clf.predict(stacked_example_features)

# 输出预测结果
print(f"Predicted target for the example case: {example_prediction[0]}")

# 验证日志
validation_log = f"Validation Log:\n"
validation_log += f"Predicted target for the example case: {example_prediction[0]}\n"
validation_log += f"Features used for prediction: {example_case}\n"

# 输出验证日志
print(validation_log)

