#查看胸痛与心脏病的关系
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
from seaborn._core import data
import matplotlib.pyplot as plt
plt.rcParams[ 'font.sans-serif' ]=[ 'SimHei']

heart = pd.read_csv("processed cleveland1.csv")

cpDf = heart[['cp','target']]
sns.countplot(data = cpDf,x ='cp',hue='target',palette='Set2')
plt.xlabel('胸痛类型\n（0=典型心绞痛；1=非典型心绞痛；2=非心绞痛；3=没有症状）',fontsize=12)
plt.ylabel('人数',fontsize=12)
plt.show()