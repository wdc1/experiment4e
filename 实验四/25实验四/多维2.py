import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# 加载预处理后的UCI心脏数据集
df = pd.read_csv("processed cleveland1.csv")

# al使用pandas的pivot_table函数创建多维立方体
cube = pd.pivot_table(df, vues='target', index=['age'], columns=['cp', 'exang'])

# 定义年龄段参考字典
age_categories = {'Children': (0, 18), 'Youth': (19, 35), 'Adults': (36, 60), 'Elderly': (61, df['age'].max())}

# 根据年龄段进行分组汇总
cube_grouped = cube.groupby(pd.cut(cube.index, bins=[age_categories[cat][0] for cat in age_categories] + [np.inf], labels=age_categories.keys())).mean()

# 绘制柱形图进行可视化分析
cube_grouped.plot(kind='bar', figsize=(10, 6))
plt.xlabel('age group')
plt.ylabel('Disease Cases')
plt.title('Heart disease cases at all ages')
plt.legend(title='Heart disease-related\n characteristics')
plt.show()

# 旋转维的方向
rotated_cube = cube_grouped.transpose()

# 绘制柱形图进行可视化分析
rotated_cube.plot(kind='bar', figsize=(10, 6))
plt.xlabel('Heart disease-related characteristics')
plt.ylabel('Disease Cases')
plt.title('Heart disease cases at all ages')
plt.legend(title='age group')
plt.show()
