#将年龄进行分段
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings

from matplotlib import colors
from seaborn._core import data
import matplotlib.pyplot as plt
plt.rcParams[ 'font.sans-serif' ]=[ 'SimHei']

data = pd.read_csv("processed cleveland1.csv")

# 由于年龄过于分散，将年龄拆分成不同段
'''
pandas.cut()用来把一组数据分割成离散的区间。
比如有年龄数据，可以使用pandas.cut将年龄数据分割成不同的年龄段并打上标签。
pandas.cut(x, bins, right=True, include_lowest=False, labels=None )
参数介绍： x：被拆分的数据数组，必须是一维的，不能用DataFrame 
          bins：被切割后的区间，比如bins=[1,2,3],则区间为(1,2)和(2,3) 
          right=True,默认包含右区间，改为False，则不包含 
          include_lowest：bool型的参数，表示区间的左边是开还是闭的，默认为false
          labels：给分割后的bins打标签，长度必须和划分后的区间长度相等，
                   比如bins=[1,2,3]，划分后有2个区间，则labels的长度必须为2。
'''

# 定义一个DataFrame接收分割后的年龄
age_distDf = pd.DataFrame()

# 用pd.cut() 将年龄进行分割
age_distDf['age_range']  = pd.cut(x = data['age'],
                                  bins = [0,18,40,60,100],
                                  include_lowest = True,right=False,
                                  labels = ["儿童",'青年','中年','老年'])

# 将原数据集的target合并到age_distDf中
age_distDf = pd.concat([age_distDf['age_range'],data['target']],axis=1)
age_distDf.head()

# 画柱状图，观察不同年龄段的人患心脏病的情况
sns.countplot(data=age_distDf,x='age_range',hue='target',palette='Set2')
plt.xlabel('年龄段')
plt.show()
# 绘制饼图比较不同年龄段人群患病情况
plt.figure(figsize = (3*5,1*4))

# 青年人患病比例
ax1 = plt.subplot(1,3,1)
youth =  age_distDf[age_distDf['age_range']=='青年']['target'].value_counts()
plt.pie(youth,explode=[0,0.05],autopct='%.2f%%',labels=['患病','正常'],colors=colors)
plt.title('青年人患病比例')

# 中年人患病比例
ax2 =  plt.subplot(1,3,2)
middle = age_distDf[age_distDf['age_range']=='中年']['target'].value_counts()
plt.pie(middle,explode=(0,0.05),autopct='%.2f%%',labels=['患病','正常'],colors=colors)
plt.title('中年人患病比例')

# 老年人患病比例
ax2 = plt.subplot(1,3,3)
old =  age_distDf[age_distDf['age_range']=='老年']['target'].value_counts()
plt.pie(old,explode=[0,0.05],autopct='%.2f%%',labels=['患病','正常'],colors=colors)
plt.title('老年人患病比例')
plt.show()

plt.rcParams['font.sans-serif'] = ['SimHei']

heart = pd.read_csv("processed cleveland1.csv")

plt.figure(figsize=(15, 10))
corr = heart.corr()
ax = sns.heatmap(data=corr,cmap=plt.cm.RdYlBu_r, annot=True, fmt='.2f')
a, b = ax.get_ylim()
ax.set_ylim(a + 0.5, b - 0.5)
plt.show()

#查看运动引起心绞痛与心脏病的关系
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
from seaborn._core import data
import matplotlib.pyplot as plt
plt.rcParams[ 'font.sans-serif' ]=[ 'SimHei']

heart = pd.read_csv("processed cleveland1.csv")

exangDf =  heart[['exang','target']]
sns.countplot(data=exangDf,x='exang',hue='target',palette='Set2')
plt.xlabel('运动是否引起心绞痛\n（0=否，1=是）',fontsize=12)
plt.ylabel('人数',fontsize=12)
plt.show()

trebpsDf = heart[['trestbps','target']]
sns.boxplot(x=trebpsDf['target'],y=trebpsDf['trestbps'])
plt.xlabel('是否患心脏病（0=否，1=是）',fontsize=12)
plt.ylabel('人数',fontsize=12)
plt.title('心脏病与静息血压的关系')
plt.show()

#查看年龄分段后的最大心率与心脏病之间的关系
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
from seaborn._core import data
import matplotlib.pyplot as plt
plt.rcParams[ 'font.sans-serif' ]=[ 'SimHei']

heart = pd.read_csv("processed cleveland1.csv")

# 心率和年龄也有一定的关系，可以结合考察心脏病，心率，年龄
thaDf = heart[['thalach','target']]
thaDf['age_range'] = pd.cut(heart['age'],bins=[0,18,40,60,100],
                           labels=['儿童','青年','中年','老年'],
                            include_lowest=True,right=False)

sns.swarmplot(data =thaDf, x='age_range',y='thalach', hue='target')
plt.xlabel('年龄段', fontsize=12)
plt.ylabel('最大心率',fontsize=12)
plt.show()