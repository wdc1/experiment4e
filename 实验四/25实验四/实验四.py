# 导入依赖库
# 导入模块
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import pydot
from six import StringIO
from sklearn import tree,model_selection,metrics
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score,roc_curve
from sklearn.metrics import confusion_matrix
from IPython.display import Image

data=pd.read_csv("processed cleveland1.csv")
data.head()
data["target"].value_counts()
countNoDisease = len(data[data.target == 0])
countHaveDisease = len(data[data.target == 1])
print("Percentage of Patients Haven't Heart Disease: {:.2f}%".format((countNoDisease / (len(data.target))*100)))
print("Percentage of Patients Have Heart Disease: {:.2f}%".format((countHaveDisease / (len(data.target))*100)))

y = data["target"].values
x = data.drop(["target"],axis=1,inplace=False)
X = data.drop(["target"],axis=1,inplace=False).values
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
print("train dataset:{0};test dataset:{1}".format(X_train.shape,X_test.shape))

# 训练决策树模型
clf = DecisionTreeClassifier(max_depth=10, min_samples_split=5, max_leaf_nodes=8)
clf.fit(X_train, y_train)

clf = DecisionTreeClassifier(max_depth=10,min_samples_split=5,max_leaf_nodes=8)
clf.fit(X_train, y_train)
train_score = clf.score(X_train,y_train)
test_score = clf.score(X_test,y_test)
print("train score:{0};test score{1}".format(train_score,test_score))

y_predict = clf.predict(X_test)
print("混淆矩阵：")
print(metrics.confusion_matrix(y_test,y_predict))
print("正确率：")
print(metrics.accuracy_score(y_test,y_predict))

mat = confusion_matrix(y_test, y_predict)
plt.figure(figsize = (10,5))
sns.heatmap(mat.T, square=True, annot=True, fmt="d", cmap="PuRd")
plt.xlabel("predicted value",fontsize = 15)
plt.ylabel("true value",fontsize = 15)
plt.show()

print(classification_report(y_test, y_predict, target_names=["Non Disease", "Disease"]))

clf.score(X_test, y_test)

#数据准备
y_prob = clf.predict_proba(X_test)[:,0]
fpr,tpr,threshold = metrics.roc_curve(y_test, y_prob, pos_label=0)
roc_auc = metrics.auc(fpr,tpr)*100

#绘制ROC曲线与AUC面积：
plt.rcParams["font.sans-serif"] = ["SimHei"]
plt.rcParams["font.family"] = ["sans-serif"]
fig,ax = plt.subplots(figsize = (8,5))
plt.plot(fpr, tpr, color = "red",lw = 2)
plt.plot([0, 1], [0, 1], color = "orange", lw = 2, linestyle = '--')
plt.text(0.6,0.5,"AUC = %0.1f %%" % roc_auc, fontsize = 15)
ax.fill_between(fpr,0, tpr, facecolor = "#e50000", alpha = 0.3)
plt.xlim([-0.1, 1.1])
plt.ylim([-0.1, 1.1])
plt.xticks([0,0.2,0.4,0.6,0.8,1])
plt.xlabel("假正率FPR",fontsize = 15)
plt.ylabel("真正率TPR",fontsize = 15)
plt.title("ROC & AUC",fontsize = 20)
plt.show()

# 绘制决策树
fig, ax = plt.subplots(figsize=(15, 10))
plot_tree(clf,
          feature_names=data.drop(['target'], axis=1).columns,  # 特征名
          class_names=['Non Disease', 'Disease'],  # 类别名
          filled=True)  # 是否填充颜色
plt.title("Decision Tree for Heart Disease Classification")
plt.show()